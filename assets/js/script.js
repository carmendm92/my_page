// SIDEBAR
const menu_toggle = document.querySelector(".menu-toggle");
const sidebar = document.querySelector(".sidebar");


menu_toggle.addEventListener('click', () => {
    menu_toggle.classList.toggle('is-active');
    sidebar.classList.toggle('close');
})

let w = window.innerWidth;
function myfunction() {
    if (w < 768) {
        sidebar.classList.toggle('close');
    }
}
myfunction();

// BOX FOTO

const boxes = document.querySelectorAll(".box");
var allBoxesLenght = boxes.length;

var indexImage = 0;
var init = function () {
    imageInterval();
    initBoxesShow();
};

init();

var images = [
    "jump",
    "lidl",
    "lidl_mascherina",
    "parapendio",
    "elegante",
    "vino",
    "pc"
];

function imageInterval() {
    setInterval(() => {
        let min = 0;
        let max = 6;
        indexImage = Math.floor(Math.random() * (max - min + 1) + min);
    }, 1000);
}

function initBoxesShow() {
    var i = 0;
    setInterval(() => {
        images.forEach(function (image) {
            boxes[i].classList.forEach(function (classImage) {
                if (image == classImage) {
                    boxes[i].classList.remove(image);
                    boxes[i].classList.add(images[indexImage]);
                }
            })
        })
        if (i < allBoxesLenght - 1) {
            i++;
        } else {
            i = 0
        }
    }, 1000);
}
//SCROLL BAR
let progressBar = document.getElementById('progressbar');
let totalHeight = document.body.scrollHeight - window.innerHeight;
window.onscroll = function () {
    let progressHeight = (window.pageYOffset / totalHeight) * 100;
    progressBar.style.height = progressHeight + "%";
}